let express = require('express');
let router = express.Router();
let trieUtils = require('../modules/trie/trieUtils.js');
let search = require('../modules/searchApp/searchService.js');
let Trie = require('../modules/trie/trie.js');
let searchMiddleware = require('./middleware/searchQueryMiddleware.js');

global.globalTrie = new Trie();
trieUtils.readFromCSV(globalTrie);

router.get('/', function(req, res) {
  res.render('index');
});

router.use('/search', searchMiddleware);

router.get('/search', function(req, res, next) {
    let start = new Date().getTime();
    let result = search.searchName(globalTrie, req.query.q);
    let resTime = new Date().getTime() - start;
    res.json({
      result,
      apiResTime: resTime
    });
    
});

module.exports = router;
