module.exports = function(req, res, next) {
    if (req.query.q.length < 3) {
        res.json({
            error: "Query term should be greater than 3"
        })
        return;
    }
    next();
}