Deep Thought : A Trie Based Name Search Engine
====================================


 This search engine takes in a search query and searches for the best possible matches(of givenName, middleName and surname) that are suffixes of the search term.

 Search results are ranked according to the following criteria:

 * Results whose givenName field begin with the search query are assigned a *primary score* of 2, and otherwise assigned a primary *primary score* of 1
 * Results whose matching suffix(givenName, middleName or surname) is shorter in length are given higher *secondary score*.
 > *secondary score* = 100 / [length of matched suffix]
* Finally, the cumulative score is calculated as :
> *final score* = *primary score* + *secondary score*


## Running the Application

### Installation
To install dependencies, run 
```bash
npm install
```
### Running the Application
To start the server, run
```bash
npm start
```
This will run on port **3000** by default

To serve on a custom port, run
```bash
PORT=[PORT] npm start
```

### Tests

Tests are located in the **test** directory in the main project folder

To run tests, use
```bash
npm start test
```

### Project Structure

#### Routing
All routing is performed from the **routes/** directory

#### Trie
The Trie Class and all associated files are located in **modules/trie/**

#### Tests
Tests are located in **test/**

#### Resources
* All stylesheets are stored in **public/stylesheets/**

* All markup templates are stored in **views/**
