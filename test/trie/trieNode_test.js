let path = require('path');
let TrieNode = require(path.join(__dirname,'../../modules/trie/trieNode.js'));

const MODULE_NAME = 'TrieNode';
let assert = require('assert');

let text = "this";

describe(MODULE_NAME, function() {
    let trieNode;
    beforeEach(() => {
        trieNode = new TrieNode();
        trieNode.insert(text, 0, 1);
    });
    describe('insert', function() {
        it('should create a child for next alphabet on insert', function() {
            assert.deepEqual(!!(trieNode.children['t']), true);
        });
        it('should set endNode flag and index if end node', function() {
            var node = new TrieNode();
            node.insert('t', 0, 1);
            assert.deepEqual(node.children['t'].isEndNode, true);
            assert.deepEqual(node.children['t'].endReference, [0]);
        });
    });
    describe('getAllChildren', function() {
        it('should get all children of node', function() {
            assert.deepEqual(trieNode.getAllChildren().length, 1);
        });
    });
    describe('search', function() {
        it('should return index on search success', function() {
            assert.deepEqual(trieNode.search('th')[0], 0);
        });
        it('should return empty result when invalid search term is used', function() {
            assert.deepEqual(trieNode.search('gh'), []);
        });
    });
});
