let path = require('path');
let Trie = require(path.join(__dirname,'../../modules/trie/trie.js'));

const MODULE_NAME = 'Trie';
let assert = require('assert');

let name = {
    'givenName': 'Bobby',
    'middleName': 'James',
    'surname': 'Fischer'
};

describe(MODULE_NAME, function() {
    let trie;
    beforeEach(() => {
        trie = new Trie();
        trie.insert(name);
    });
    describe('insert', function() {
        it('add name into value queue on insert', function() {
            assert.deepEqual(trie.values[0], name);
        });
    });
    describe('search', function() {
        it('should return valid index on searching', function() {
            assert.deepEqual(trie.values[trie.search('bob')[0].result], name);
        });
        it('should return empty result on invalid search', function() {
            assert.strictEqual(trie.search('bobfg').length, 0);
        });
    });
});
