let path = require('path');
let searchUtils = require(path.join(__dirname,'../../modules/searchApp/searchUtils.js'));

const MODULE_NAME = 'searchUtils'
let assert = require('assert');

let name = {
    'givenName': 'Bobby',
    'middleName': 'James',
    'surname': 'Fischer'
};
let longName = {
    'givenName': 'BobbyLongName',
    'middleName': 'James',
    'surname': 'Fischer'
};


describe(MODULE_NAME, function() {
    describe('getSearchScore', function() {
        it('high score when result starts with query', function() {
            assert.deepEqual(searchUtils.getSearchScore('bobby', name), 2.2);
        });
        it('low score when results doesnt start with query', function() {
            assert.deepEqual(searchUtils.getSearchScore('james', name), 1.2);
        });
        it('low score for higher length results', function() {
            assert.strictEqual(searchUtils.getSearchScore('bob', longName) <  searchUtils.getSearchScore('bob', name), true);
        });
    });
});
