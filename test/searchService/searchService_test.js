let path = require('path');
let search = require(path.join(__dirname,'../../modules/searchApp/searchService.js'));
let Trie = require(path.join(__dirname,'../../modules/trie/trie.js'));

const MODULE_NAME = 'searchService'
let assert = require('assert');

let name = {
    'givenName': 'Bobby',
    'middleName': 'James',
    'surname': 'Fischer'
};

describe(MODULE_NAME, function() {
    let trie;
    beforeEach(() => {
        trie = new Trie();
        trie.insert(name);
    });
    describe('searchName', function() {
        it('should return result when valid name is searched', function() {
            assert.deepEqual(search.searchName(trie, 'Bobby')[0].result, name);
        });
        it('should result when a valid prefix is searched', function() {
            assert.deepEqual(search.searchName(trie, 'Bob')[0].result, name);
        });
  });
});
