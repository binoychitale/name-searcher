module.exports = {
  scripts: {
    default: 'node ./bin/www',
    test: 'mocha -- test/**/*.js'
  }
};
