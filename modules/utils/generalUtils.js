module.exports = {
    isNotNullOrEmpty: function (value) {
        if (typeof value === 'object') return !this.isEmpty(value);

        return !!(value);
    },
    isNullOrEmpty: function (value) {
        return !this.isNotNullOrEmpty(value);
    },
    isEmpty: function (obj) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
}