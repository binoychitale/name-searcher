module.exports = {
    get: function (array, ...indices) {
        return indices.reduce(function (prev, current) {
            return prev && prev[current];
        }, array) || null;
    }
} 