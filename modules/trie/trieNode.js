let TrieNode = function() {
    this.children = {};
    this.isEndNode = false;
    this.endReference = [];
    this.lowerNodes = [];
}

TrieNode.prototype.insert = function(subseq, wordIndex, depth) {
    if (subseq.length == 0) {
        this.isEndNode = true;
        this.endReference.push(wordIndex);
        return;
    }
    let nextNode = this.children[subseq[0]];
    if(nextNode) {
        nextNode.insert(subseq.substring(1), wordIndex, depth+1);
    } else {
        let newNode = new TrieNode();
        this.children[subseq[0]] = newNode;
        newNode.insert(subseq.substring(1), wordIndex, depth+1);
    }
}


TrieNode.prototype.getAllChildren = function() {
    let children = Object.keys(this.children).map((key) => this.children[key], this)
        .reduce((result, child) => {
            return result.concat(child.getAllChildren());
        }, []);
    if (this.isEndNode) {
        return children.concat(this.endReference);
    }
    return children;
}

TrieNode.prototype.search = function(subkey) {
    if (subkey.length == 0) {
        return this.getAllChildren();
    } else {
        let child = this.children[subkey[0]]
        if(child) {
            return child.search(subkey.substring(1))
        }
        return [];
    }
}

module.exports = TrieNode;