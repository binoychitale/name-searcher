let TrieNode = require('./trieNode.js');
var searchUtils = require('../searchApp/searchUtils.js');

let Trie = function () {
    this.head = new TrieNode();
    this.values =  [];
}

Trie.prototype.insert = function(name) {
    this.values.push(name);
    name['givenName'] && this.head.insert(name['givenName'].toLowerCase(), this.values.length - 1, 1);
    name['middleName'] && this.head.insert(name['middleName'].toLowerCase(), this.values.length - 1, 1);
    name['surname'] && this.head.insert(name['surname'].toLowerCase(), this.values.length - 1, 1);
}

Trie.prototype.search = function (query) {
    if (!query) {
        return [];
    }
    let results = this.head.search(query);
    results = results.map((result) => {
        return {
            result,
            score: searchUtils.getSearchScore(query, this.values[result])
        };
    });

    return results;
}

module.exports = Trie;