let csv = require('fast-csv');
let path = require('path');

function readFromCSV(trie) {
    csv.fromPath(path.join(__dirname, "data.csv"), {headers: true})
    .on("data", data => {
        trie.insert(data);
    });
}

module.exports = {
    readFromCSV
};