let searchUtils = require('./searchUtils.js');

module.exports = {
    searchName: function(trie, query) {
        let searchTerms = searchUtils.splitSearchQuery(query);
        let results = [];
        searchTerms.map((term) => {
            results = results.concat(trie.search(term.toLowerCase()));
        });
        let countMap = {};
        results.map((res) => {
            if (!countMap[res.result]) {
                countMap[res.result] = {};
            }
            countMap[res.result].count = countMap[res.result].count ? countMap[res.result].count + 1 : 1;
            countMap[res.result].score = countMap[res.result].score ? Math.max(countMap[res.result].score, res.score) : 0;
        });
        results = results.filter((res) => {
            if (countMap[res.result].count === searchTerms.length) {
                countMap[res.result].count = 0;
                return true;
            }
        });
        return results
        .map((searchResult) => {
            return {
                result: trie.values[searchResult.result],
                score: searchResult.score
            }
        },this)
        .sort(searchUtils.compareSearchScore);
    }
}