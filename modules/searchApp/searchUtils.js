module.exports = {
    getSearchScore: function(query, name) {
        let primaryScore = 1;
        if (name.givenName && name.givenName.toLowerCase().indexOf(query) === 0) {
            primaryScore = 2;
        }
        let secondaryScore = 0;
        if (name.givenName && name.givenName.toLowerCase().indexOf(query) === 0) {
            secondaryScore = Math.max(secondaryScore, 100 / name.givenName.length);
        }
        if (name.middleName && name.middleName.toLowerCase().indexOf(query) === 0) {
            secondaryScore = Math.max(100 / name.middleName.length);
        }
        if (name.surname && name.surname.toLowerCase().indexOf(query) === 0) {
            secondaryScore = Math.max(secondaryScore, 100 / name.surname.length);
        }

        return primaryScore + secondaryScore / 100;
    },
    compareSearchScore: function(a, b) {
        return b.score - a.score;
    },
    splitSearchQuery: function(query) {
        return query.split(' ');
    }
}